from setuptools import setup

setup(
    name="homework_exercise",
    version="0.1",
    py_modules=["_calculation", "_data"]
)
